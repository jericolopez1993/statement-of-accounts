module Kilatis
  class AccountsController < ApplicationController
    before_action :set_account, only: [:show, :edit, :update, :destroy]
    helper_method :grey_page
    layout "print_layout", :only => [ :print, :print_sales_report ]

    # GET /accounts
    # GET /accounts.json
    def index
      @client = KilatisClient.order("lname ASC, fname ASC").first
      @accounts = []
      if @client.present?
        @accounts = KilatisAccount.where(:id_client => @client.id, :yr => @year_today, :mon => @month_today).order("day ASC")
      end
      @selected_month = @month_today
      @selected_year = @year_today
      get_latest_account(@client&.id)
    end
    # GET /accounts/1
    # GET /accounts/1.json
    def show
    end

    # GET /accounts/new
    def new
      @client = KilatisClient.find(params[:id])
      @accounts = []
      if @client.present?
        @accounts = KilatisAccount.where(:id_client => @client.id, :yr => @year_today, :mon => @month_today).order("day ASC")
      end
      @selected_month = params[:mon]
      @selected_year = params[:yr]
      get_latest_account(@client&.id)
      @account = KilatisAccount.new
    end

    # GET /accounts/1/edit
    def edit
      @client = KilatisClient.find(@account.id_client)
      @accounts = KilatisAccount.where("id != #{@account.id} AND id_client = #{@client.id} AND yr = #{@account.yr} AND mon = #{@account.mon}").order("day ASC")
      @selected_month = @account.mon
      @selected_year = @account.yr
      get_latest_account(@client&.id)
    end

    # POST /accounts
    # POST /accounts.json
    def create
      @account = KilatisAccount.new(account_params)
      @account.user_id = session[:user_logged_id]
      @client = KilatisClient.find(@account.id_client)
      @accounts = KilatisAccount.where(:id_client => @client.id, :yr => @account.yr, :mon => @account.mon).order("day ASC")
      @selected_month = @account.mon
      @selected_year = @account.yr
      get_latest_account(@client&.id)
      if @account.save
        render "index"
      else
        render "new"

      end
    end

    # PATCH/PUT /accounts/1
    # PATCH/PUT /accounts/1.json
    def update
      @account.user_id = session[:user_logged_id]
      @client = KilatisClient.find(@account.id_client)
      @accounts = KilatisAccount.where(:id_client => @client.id, :yr => @account.yr, :mon => @account.mon).order("day ASC")
      @selected_month = @account.mon
      @selected_year = @account.yr
      get_latest_account(@client&.id)
      if @account.update(account_params)
        render "index"
      else
        render "edit"
      end
    end

    # DELETE /accounts/1
    # DELETE /accounts/1.json
    def destroy
      @client = KilatisClient.find(@account.id_client)
      @accounts = KilatisAccount.where(:id_client => @client.id, :yr => @account.yr, :mon => @account.mon).order("day ASC")
      @selected_month = @account.mon
      @selected_year = @account.yr
      get_latest_account(@client&.id)

      archive_account = KilatisArchiveAccount.new
      archive_account.id = @account.id
      archive_account.id_client = @account.id_client
      archive_account.yr = @account.yr
      archive_account.mon = @account.mon
      archive_account.day = @account.day
      archive_account.po = @account.po
      archive_account.re = @account.re
      archive_account.sold = @account.sold
      archive_account.price = @account.price
      archive_account.tcash = @account.tcash
      archive_account.tamount = @account.tamount
      archive_account.bal = @account.bal
      archive_account.date_paid = @account.date_paid
      archive_account.paid = @account.paid
      archive_account.bank_name = @account.bank_name
      archive_account.check_num = @account.check_num
      archive_account.date_issued = @account.date_issued
      archive_account.rsnum = @account.rsnum
      archive_account.prnum = @account.prnum
      archive_account.user_id = session[:user_logged_id]
      archive_account.save

      @account.destroy
      render "index"

    end

    def search
      @date = Date.today
      @client = KilatisClient.find(params[:id_client])
      @accounts = KilatisAccount.where(:id_client => params[:id_client], :yr => @year_today, :mon => @month_today).order("day ASC")
      @selected_month = @month_today
      @selected_year = @year_today
      get_latest_account(@client&.id)
      render "index"
    end

    def search_by_name
      @date = Date.today
      @client = KilatisClient.where(:lname => params[:lname], :fname => params[:fname]).first
      @accounts = KilatisAccount.where(:id_client => @client.id, :yr => @year_today, :mon => @month_today).order("day ASC")
      @selected_month = @month_today
      @selected_year = @year_today
      get_latest_account(@client&.id)
      render "index"
    end

    def search_by_month
      @client = KilatisClient.find(params[:client_id])
      @accounts = KilatisAccount.where("id_client = #{params[:client_id]} AND yr = #{params[:yr]} AND mon = #{params[:mon]}").order("yr DESC, mon DESC, day ASC")
      @selected_month = params[:mon]
      @selected_year = params[:yr]
      get_latest_account(@client&.id)
      render "index"
    end


    def print
      @client = KilatisClient.find(params[:id])
      @accounts = KilatisAccount.select("*, IFNULL(STR_TO_DATE(date_paid,'%m/%d/%Y'), CAST(date_paid AS DATE)) as latest_date_paid").where("id_client = #{params[:id]} AND yr = #{params[:yr]} AND mon = #{params[:mon]}").order("yr DESC, mon DESC, day ASC")
      @account_mon_sum = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal").where("id_client = #{params[:id]} AND yr = #{params[:yr]} AND mon = #{params[:mon]}")
      @account_overall_sum =  KilatisAccount.select("SUM(bal) AS bal").where("id_client = #{params[:id]}")
      @selected_month = params[:mon]
      @selected_year = params[:yr]
    end

    def print_sales_report
      @accounts = KilatisAccount.select("
                    CONCAT(kilatis_clients.lname, ', ', kilatis_clients.fname) as client_name,
                    SUM(po) AS po,
                    SUM(re) AS re,
                    SUM(sold) AS sold,
                    SUM(tamount) AS tamount,
                    SUM(tcash) AS tcash,
                    SUM(bal) AS bal
                  ").joins("
                    LEFT JOIN kilatis_clients ON kilatis_clients.id = kilatis_accounts.id_client
                  ").where("
                    yr = #{params[:yr]}
                    AND mon = #{params[:mon]}
                  ").group("
                    id_client
                  ").order("
                    CONCAT(kilatis_clients.lname, ', ', kilatis_clients.fname) ASC
                  ")
      @account_mon_sum = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal").where("yr = #{params[:yr]} AND mon = #{params[:mon]}")
      @account_overall_sum =  KilatisAccount.select("SUM(bal) AS bal")
      @selected_month = params[:mon]
      @selected_year = params[:yr]
    end

    def latest_summary_report
      @accounts = KilatisAccount.select("
                    CONCAT(kilatis_clients.lname, ', ', kilatis_clients.fname) as client_name,
                    SUM(po) AS po,
                    SUM(re) AS re,
                    SUM(sold) AS sold,
                    SUM(tamount) AS tamount,
                    SUM(tcash) AS tcash,
                    SUM(bal) AS bal,
                    (SELECT STR_TO_DATE(date_paid,'%m/%d/%Y') FROM kilatis_accounts AS temp_accounts WHERE temp_accounts.id_client = kilatis_accounts.id_client ORDER BY STR_TO_DATE(temp_accounts.date_paid,'%m/%d/%Y') DESC LIMIT 1) as date_paid
                  ").joins("
                    LEFT JOIN kilatis_clients ON kilatis_clients.id = kilatis_accounts.id_client
                  ").where("
                    yr = #{params[:yr]}
                    AND mon = #{params[:mon]}
                  ").group("
                    id_client
                  ").order("
                    CONCAT(kilatis_clients.lname, ', ', kilatis_clients.fname) ASC,
                    date_paid DESC
                  ")
      @account_mon_sum = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal").where("yr = #{params[:yr]} AND mon = #{params[:mon]}")
      @account_overall_sum =  KilatisAccount.select("SUM(bal) AS bal")
      @selected_month = params[:mon]
      @selected_year = params[:yr]

    end

    def generate_pdf
      @clients = KilatisClient.all.order("lname ASC, fname ASC")
      @accounts = KilatisAccount.select("*, IFNULL(STR_TO_DATE(date_paid,'%m/%d/%Y'), CAST(date_paid AS DATE)) as latest_date_paid").where("yr = #{params[:yr]} AND mon = #{params[:mon]}").order("yr DESC, mon DESC, day ASC")
      @account_mon_sum = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal, id_client").where("yr = #{params[:yr]} AND mon = #{params[:mon]}").group("id_client")
      @account_overall_sum = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal, id_client").group("id_client")
      respond_to do |format|
        format.html{ render :nothing => true }
        format.pdf do
          pdf = Prawn::Document.new(:page_layout => :landscape, :page_size => 'A2')

          @clients.each do |client|
            pdf.font_size 20
            pdf.text "Kilatis / JV-GO PUBLISHING HOUSE INC.", :align => :center
            pdf.move_down 30
            pdf.text "#{client.lname}, #{client.fname} #{client.mi}", :align => :left
            pdf.text "#{client.address}", :align => :left
            pdf.move_down 30
            pdf.font_size 16
            pdf.text "Data:", :align => :left
            pdf.font_size 12
            accounts = [["Day", "P.O", "Re", "Sold", "Price", "T.Amount", "T.Cash", "Bal", "D.Paid", "Paid", "Type", "Bank Name", "#", "Issued", "R.S. #", "P.R. #"]]
            @accounts.each do |account|
              if client.id == account.id_client
                acc = []
                acc << account.day
                acc << account.po
                acc << account.re
                acc << account.sold
                acc << account.price
                acc << account.tamount
                acc << account.tcash
                acc << account.bal
                acc << check_for_two_dates(account.latest_date_paid, account.date_paid)
                if account.paid == true
                  acc << "YES"
                else
                  acc << ""
                end
                acc << account.payment_type
                acc << account.bank_name
                acc << account.check_num
                acc << account.date_issued
                acc << account.rsnum
                acc << account.prnum

                accounts << acc
              end
            end
            pdf.table(accounts, :width => 1200, :position => :center)
            pdf.move_down 30
            pdf.font_size 16
            pdf.text "Total: (Month Only)", :align => :left
            pdf.font_size 12
            mon_accounts = [["Per Order", "Return", "Sold", "Total Amount", "Total Cash", "Balance"]]
            @account_mon_sum.each do |account|
              if client.id == account.id_client
                acc = []
                acc << account.po
                acc << account.re
                acc << account.sold
                acc << account.tamount
                acc << account.tcash
                acc << account.bal
                mon_accounts << acc
              end
            end
            pdf.table(mon_accounts, :width => 1200, :position => :center)
            pdf.move_down 30
            pdf.font_size 16
            pdf.text "Total: (Overall)", :align => :left
            pdf.font_size 12
            over_accounts = [["Per Order", "Return", "Sold", "Total Amount", "Total Cash", "Balance"]]
            @account_overall_sum.each do |account|
              if client.id == account.id_client
                acc = []
                acc << account.po
                acc << account.re
                acc << account.sold
                acc << account.tamount
                acc << account.tcash
                acc << account.bal
                over_accounts << acc
              end
            end
            pdf.table(over_accounts, :width => 1200, :position => :center)
            pdf.start_new_page
          end
          send_data pdf.render, filename: 'report.pdf', type: 'application/pdf'
        end
      end
    end

    def get_totals
      get_client_totals(params[:client_id],params[:selected_month],params[:selected_year])
      @selected_month = params[:selected_month]
      @selected_year = params[:selected_year]
      render :layout => false
    end

    def get_search_client
      render :layout => false
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_account
      @account = KilatisAccount.find(params[:id])
    end

    def get_client_totals(id, mon, year)
      @account_month = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal").where("id_client = #{id} AND yr = #{year} AND mon = #{mon}")
      @account_year = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal").where("id_client = #{id} AND yr = #{year}")
      @account_overall = KilatisAccount.select("SUM(po) AS po, SUM(re) AS re, SUM(sold) AS sold, SUM(tamount) AS tamount, SUM(tcash) AS tcash, SUM(bal) AS bal").where("id_client = #{id}")
    end

    def get_latest_account(id)
      unless id.present?
        return nil
      end
      @latest_account = KilatisAccount.select("*, IFNULL(STR_TO_DATE(date_paid,'%m/%d/%Y'), CAST(date_paid AS DATE)) as latest_date_paid").where("id_client = #{id}").order("IFNULL(STR_TO_DATE(date_paid,'%m/%d/%Y'), CAST(date_paid AS DATE)) DESC").first
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def account_params
      params[:kilatis_account][:sold] = params[:kilatis_account][:po].to_i - params[:kilatis_account][:re].to_i
      params[:kilatis_account][:tamount] = params[:kilatis_account][:price].to_f * params[:kilatis_account][:sold].to_f
      params[:kilatis_account][:bal] = params[:kilatis_account][:tamount].to_f - params[:kilatis_account][:tcash].to_f
      params.require(:kilatis_account).permit(:id_client, :yr, :mon, :day, :po, :re, :sold, :price, :tcash, :tamount, :bal, :date_paid, :paid, :rsnum, :prnum, :addbal, :bank_name, :check_num, :date_issued, :payment_type)
    end

  end
end
