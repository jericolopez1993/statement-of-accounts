class ReportsController < ApplicationController
  layout "print_layout", :only => [ :generate ]
  def index
    @clients = SagadClient.find_by_sql("SELECT DISTINCT clients.lname, clients.fname FROM (SELECT sagad_clients.fname, sagad_clients.lname
                                                        FROM sagad_clients
                                                        UNION SELECT kadyot_clients.fname, kadyot_clients.lname
                                                        FROM kadyot_clients) AS clients
                                       ORDER BY clients.lname ASC")
  end

  def generate
    @report_type = params[:report_type]
    @client_names = convert_array(params[:client_name]).split(',')
    default_daterange = params[:default_daterange].split(" - ")
    @daterange = (default_daterange[0].to_date...default_daterange[1].to_date).to_a
    @daterange.push(default_daterange[1].to_date)
    #@sagad_accounts = []
    #@kadyot_accounts = []
    #@viral_accounts = []
    @sagad_accounts = generate_accounts("sagad")
    @kadyot_accounts = generate_accounts("kadyot")
    @viral_accounts = generate_accounts("viral")
    puts "#{@sagad_accounts.to_json}"
    puts "#{@kadyot_accounts.to_json}"
    puts "#{@viral_accounts.to_json}"
    render "print"
  end

  private

    def generate_accounts(newspaper)
      client_class = "#{newspaper}_clients".singularize.classify.constantize
      query_where = ""
      query_select = ""
      count_number = 1
      @client_names.each do |name|
        full_name = name.split('/')
        if @client_names[0] != name
          query_where = query_where + " OR "
        end
        query_where = query_where + "(#{newspaper}_clients.lname LIKE '#{full_name[0]}' AND #{newspaper}_clients.fname LIKE '#{full_name[1]}')"

      end
      @daterange.each do |current_date|
        query_select = query_select + "(SELECT SUM(bal) FROM #{newspaper}_accounts WHERE #{newspaper}_accounts.id_client = #{newspaper}_clients.id AND CAST(CONCAT(#{newspaper}_accounts.yr, '-', #{newspaper}_accounts.mon, '-', #{newspaper}_accounts.day) AS DATE) <= '#{current_date.strftime('%Y-%m-%d')}') AS balance_#{count_number}, "
        query_select = query_select + "(SELECT IFNULL(STR_TO_DATE(#{newspaper}_accounts.date_paid,'%m/%d/%Y'), CAST(#{newspaper}_accounts.date_paid AS DATE)) FROM #{newspaper}_accounts WHERE #{newspaper}_accounts.id_client = #{newspaper}_clients.id AND CAST(CONCAT(#{newspaper}_accounts.yr, '-', #{newspaper}_accounts.mon, '-', #{newspaper}_accounts.day) AS DATE) <= '#{current_date.strftime('%Y-%m-%d')}' ORDER BY IFNULL(STR_TO_DATE(#{newspaper}_accounts.date_paid,'%m/%d/%Y'), CAST(#{newspaper}_accounts.date_paid AS DATE)) DESC LIMIT 1) AS date_paid_#{count_number}, "
        count_number = count_number + 1
      end
      return client_class.select("
                    #{newspaper}_clients.id,
                    #{query_select}
                    CONCAT(#{newspaper}_clients.lname, ', ', #{newspaper}_clients.fname) as client_name
                  ").where(query_where)
    end
end
