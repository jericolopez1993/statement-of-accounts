class UsersController < ApplicationController
  before_action :set_user, only: [:show, :edit, :update, :destroy]
  # GET /users
  # GET /users.json
  def index
    @users = User.all
  end

  # GET /users/1
  # GET /users/1.json
  def show
  end

  # GET /users/new
  def new
    @user = User.new
  end

  # GET /users/1/edit
  def edit
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        save_log("New Create", @user.id)
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render :show, status: :created, location: @user }
      else
        format.html { render :new }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    respond_to do |format|
      if @user.update(user_params)
        save_log("Update/Edit", @user.id)

        if @sec_level.name == "Administrator"
          format.html { redirect_to users_path, notice: 'User was successfully updated.' }
        else
          format.html { redirect_to accounts_path, notice: 'User was successfully updated.' }
        end
        format.json { render :show, status: :ok, location: @user }
      else
        format.html { render :edit }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
    save_log("Delete", @user.id)
    respond_to do |format|
      format.html { redirect_to users_path, notice: 'User was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  def login
    if session[:user_logged_id].present? && !session[:user_logged_id].nil?
      if @sec_level.name == "Administrator"
        redirect_to users_path
      elsif @sec_level.name == "Bagong Sagad" || @sec_level.name == "Kadyot"
        redirect_to accounts_path
      else
        redirect_to logs_path
      end
    end
  end
  #Authenticate User Login
  def authenticate
    email = params[:email]
    password = params[:password]
    user_email = User.where({:email => email,:password=> Digest::MD5.hexdigest(password)}).first
    puts "#{user_email.to_json}"
    if user_email.present?
      user = user_email
    end

    if user.present?
      session[:email] = user.email
      session[:name] = user.fname+" "+user.lname
      session[:user_logged_id] = user.id
      session[:sec_id] = user.sec_level_id
      session[:position] = user.position
      session[:created_at] = user.created_at.strftime('%B %Y')
      session[:time_logged] = Time.now + 12.hour
      @sec_level = SecLevel.find(user.sec_level_id)
      if @sec_level.name == "Administrator"
        redirect_to users_path
      elsif @sec_level.name == "Bagong Sagad" || @sec_level.name == "Kadyot"
        redirect_to accounts_path
      else
        redirect_to logs_path
      end

    else
      flash[:notice] = "Invalid Credentials, Please try again."
      redirect_to login_users_path
    end
  end

  def logout
    session[:email] = nil
    session[:name] = nil
    session[:user_logged_id] = nil
    session[:sec_id] = nil
    session[:time_logged] = nil
    UserMailer.report_email(session[:full_newspaper], Date.today.strftime('%B %d %Y')).deliver
    redirect_to login_users_path
  end



  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:fname, :lname, :mi, :position, :sec_level_id, :username, :password, :email, :profile)
    end
end
