module Kadyot
  class ClientsController < ApplicationController
    before_action :set_client, only: [:show, :edit, :update, :destroy]
    # GET /clients
    # GET /clients.json
    def index
      @clients = KadyotClient.all
    end

    # GET /clients/1
    # GET /clients/1.json
    def show
    end

    # GET /clients/new
    def new
      @client = KadyotClient.new
    end

    # GET /clients/1/edit
    def edit
    end

    # POST /clients
    # POST /clients.json
    def create
      @client = KadyotClient.new(client_params)

      respond_to do |format|
        if @client.save
          format.html { redirect_to @client, notice: 'Client was successfully created.' }
          format.json { render :show, status: :created, location: @client }
        else
          format.html { render :new }
          format.json { render json: @client.errors, status: :unprocessable_entity }
        end
      end
    end

    # PATCH/PUT /clients/1
    # PATCH/PUT /clients/1.json
    def update
      respond_to do |format|
        if @client.update(client_params)
          format.html { redirect_to @client, notice: 'Client was successfully updated.' }
          format.json { render :show, status: :ok, location: @client }
        else
          format.html { render :edit }
          format.json { render json: @client.errors, status: :unprocessable_entity }
        end
      end
    end

    # DELETE /clients/1
    # DELETE /clients/1.json
    def destroy
      archive_client = KadyotArchiveClient.new
      archive_client.id = @client.id
      archive_client.lname = @client.lname
      archive_client.fname = @client.fname
      archive_client.mi = @client.mi
      archive_client.pnum = @client.pnum
      archive_client.tnum = @client.tnum
      archive_client.user_id = session[:user_logged_id]
      archive_client.save
      @client.destroy
      respond_to do |format|
        format.html { redirect_to kadyot_clients_url, notice: 'Client was successfully destroyed.' }
        format.json { head :no_content }
      end
    end

    private
    # Use callbacks to share common setup or constraints between actions.
    def set_client
      @client = KadyotClient.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def client_params
      params.require(:kadyot_client).permit(:lname, :fname, :mi, :pnum, :tnum, :address)
    end
  end

end
