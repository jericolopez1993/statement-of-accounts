class ApplicationController < ActionController::Base
  include ApplicationHelper
  include Pundit
  before_action :authenticate_user!
  before_action :configure_permitted_parameters, if: :devise_controller?
  layout :layout_by_resource

  before_action :global_price, :only => [:edit]
  before_action :global_date

  def global_price
    if params[:controller].include? "sagad"
      @prices = SagadPrice.all.order("price DESC")
    elsif params[:controller].include? "kadyot"
      @prices = KadyotPrice.all.order("price DESC")
    elsif params[:controller].include? "viral"
      @prices = ViralPrice.all.order("price DESC")
    elsif params[:controller].include? "kilatis"
      @prices = KilatisPrice.all.order("price DESC")
    end
  end

  def global_client
    if params[:controller] == "accounts"
      @clients = Client.all
    end
  end

  def global_date
    @date = Date.today
    @month_today = @date.strftime("%-m")
    @year_today = @date.strftime("%Y")
  end

  private
    def user_not_authorized
      flash[:error] = "You are not authorized to perform this action."
      redirect_to(request.referrer || authenticated_root_path)
    end

    def layout_by_resource
      if devise_controller?
        "devise"
      else
        "application"
      end
    end

  protected
    def after_sign_in_path_for(resource)
      if current_user.has_role?(:admin)
        request.env['omniauth.origin'] || stored_location_for(resource) || authenticated_root_path
      else
        authenticated_root_path
      end
    end

    def configure_permitted_parameters
      devise_parameter_sanitizer.permit(:sign_up, keys: [:first_name, :last_name, :avatar])
      devise_parameter_sanitizer.permit(:account_update, keys: [:first_name, :last_name, :avatar])
    end

end
