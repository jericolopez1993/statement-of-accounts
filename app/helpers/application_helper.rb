module ApplicationHelper
  include ActionView::Helpers::NumberHelper

  def get_month_name mon
    if mon == '1'
      "January"
    elsif mon == '2'
      "February"
    elsif mon == '3'
      "March"
    elsif mon == '4'
      "April"
    elsif mon == '5'
      "May"
    elsif mon == '6'
      "June"
    elsif mon == '7'
      "July"
    elsif mon == '8'
      "August"
    elsif mon == '9'
      "September"
    elsif mon == '10'
      "October"
    elsif mon == '11'
      "November"
    elsif mon == '12'
      "December"
    end
  end

  def beautify_numbers num
    if num
      number_with_delimiter(num, :delimiter => ',')
    else
      0
    end
  end

  def return_profile_picture size
    user = User.find(session[:user_logged_id])
    if size == "medium"
      user.profile.url(:med)
    elsif size == "large"
      user.profile.url(:large)
    else
      user.profile.url(:small)
    end
  end

  def return_month_name num
    if num == 1 || num == "1"
      "January"
    elsif num == 2 || num == "2"
      "February"
    elsif num == 3 || num == "3"
      "March"
    elsif num == 4 || num == "4"
      "April"
    elsif num == 5 || num == "5"
      "May"
    elsif num == 6 || num == "6"
      "June"
    elsif num == 7 || num == "7"
      "July"
    elsif num == 8 || num == "8"
      "August"
    elsif num == 9 || num == "9"
      "September"
    elsif num == 10 || num == "10"
      "October"
    elsif num == 11 || num == "11"
      "November"
    elsif num == 12 || num == "12"
      "December"
    end
  end

  def return_user_fullname id
    user = User.find(id)
    user.lname + ", " + user.fname + " " + user.mi
  end

  def get_sale_percentage po, re
    if po.present? && re.present?
      po = po.to_f
      re = re.to_f
      return (((po-re)/po)* 100).round(2)
    else
      return 0
    end
  end
  def get_return_percentage po, re
    if po.present? && re.present?
      po = po.to_f
      re = re.to_f
      return ((re/po)* 100).round(2)
    else
      return 0
    end
  end

  def get_date_format format_date
    if !format_date.nil? && format_date.present? && !format_date.blank?
      begin
        split_date = format_date.split('/')
        if split_date.length == 3
          format_date = "#{split_date[2]}-#{split_date[0]}-#{split_date[1]}"
        end
        format_date.to_date.strftime('%m/%d/%Y').to_s
      rescue
        begin
          Date.strptime(format_date, '%m/%d/%Y').to_date.strftime('%m/%d/%Y')
        rescue
          ''
        end
      end
    else
      ''
    end
  end

  def check_for_two_dates date1, date2
    if date1
      return date1.to_date.strftime('%m/%d/%Y')
    else
      get_date_format(date2)
    end
  end

  def format_client_name lname, fname
    full_name = "#{lname}, #{fname}"
    full_name.titleize
  end

  def convert_array(arr)
    begin
      arr.to_s.tr('[]', '').tr('"', '')[2..-1].gsub(', ', ',').to_s
    rescue
      ""
    end
  end

  def get_accounts(name, accounts)
    accounts.each do |account|
      if account.client_name.upcase == name.upcase
        return account
      end
    end
    return nil
  end

end
