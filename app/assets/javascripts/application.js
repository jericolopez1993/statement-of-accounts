// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require rails-ujs
//= require activestorage
//= require vendor/app.min
//= require vendor/default.min
//= require vendor/dashboard-v3.js
//= require vendor/table-manage-buttons.demo
//= require vendor/render.highlight
//= require vendor/form-slider-switcher.demo
//= require vendor/map-google.demo
//= require vendor/countries
//= require vendor/depends_on
//= require vendor/email-inbox.demo
//= require vendor/jquery-jvectormap.min
//= require vendor/jquery-jvectormap-world-mill
//= require vendor/daterangepicker
//= require inputmask
//= require jquery.inputmask
//= require inputmask.extensions
//= require inputmask.date.extensions
//= require inputmask.phone.extensions
//= require inputmask.numeric.extensions
//= require inputmask.regex.extensions
//= require tinymce
//= require select2
//= require drivers
//= require users
//= require_tree .


function isValidated() {
  var fields = $('input[required="required"], select[required="required"]');
  var isValidate = true;
  for(var i=0;i<fields.length;i++){
    if(!$(fields[i]).val() || 0 === $(fields[i]).val().length){
      isValidate = false;
      break;
    }
  }
  return isValidate;
}

$(function() {
  keypress_fields();
});


$(document).ready(function() {
  $(".static-dropdown").select2({allowClear: false});
  $(".multi-static-dropdown").select2({ placeholder: "Select Clients", allowClear: false });
  $('.datepicker-autoClose').datepicker({
    autoclose: true,
    orientation: 'auto bottom'
  });
  $('.table-as-datatable').DataTable({
    "pageLength": 50,
    "drawCallback": function( settings ) {
      $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover',
        html: true
      })
    },
  });

  //Show Totals
  $('#sagad_show_totals').on('click', function(e) {
    var element_id = "sagad_account_total";
    var client_id = $(this).data( "client_id" );
    var selected_year = $(this).data( "selected_year" );
    var selected_month = $(this).data( "selected_month" );
    var url = "/sagad/accounts/get_totals";
    getTotals(url, element_id, client_id, selected_year, selected_month);
  });
  $('#kadyot_show_totals').on('click', function(e) {
    var element_id = "kadyot_account_total";
    var client_id = $(this).data( "client_id" );
    var selected_year = $(this).data( "selected_year" );
    var selected_month = $(this).data( "selected_month" );
    var url = "/kadyot/accounts/get_totals";
    getTotals(url, element_id, client_id, selected_year, selected_month);
  });
  $('#viral_show_totals').on('click', function(e) {
    var element_id = "viral_account_total";
    var client_id = $(this).data( "client_id" );
    var selected_year = $(this).data( "selected_year" );
    var selected_month = $(this).data( "selected_month" );
    var url = "/viral/accounts/get_totals";
    getTotals(url, element_id, client_id, selected_year, selected_month);
  });

  //Search by Client
  $('#sagad_search').on('click', function(e) {
    var element_id = "sagad_client_search";
    var url = "/sagad/accounts/get_search_client";
    getSearchClients(url, element_id);
  });
  $('#kadyot_search').on('click', function(e) {
    var element_id = "kadyot_client_search";
    var url = "/kadyot/accounts/get_search_client";
    getSearchClients(url, element_id);
  });
  $('#viral_search').on('click', function(e) {
    var element_id = "viral_client_search";
    var url = "/viral/accounts/get_search_client";
    getSearchClients(url, element_id);
  });
});

function getTotals(url, element_id, client_id, selected_year, selected_month) {
  $.ajax({
    method: 'get',
    url: url,
    data: {client_id: client_id, selected_year: selected_year,  selected_month: selected_month}
  }).done(function(data) {
    $("#" + element_id + "_content").html(data);
    $("#" + element_id + "_modal").modal("show");
    $(".static-dropdown").select2({allowClear: false});
  })
}

function getSearchClients(url, element_id) {
  $.ajax({
    method: 'get',
    url: url
  }).done(function(data) {
    $("#" + element_id + "_content").html(data);
    $("#" + element_id + "_modal").modal("show");
  })
}

function compute(){
  var po = $('#account_po').val();
  var re = $('#account_re').val();
  $('#account_sold').val(po-re);

  var sold = $('#account_sold').val();
  var price = $('#account_price').val();
  $('#account_tamount').val(price*sold);

  var tamount =  $('#account_tamount').val();
  var tcash = $('#account_tcash').val();
  $('#account_bal').val(tamount-tcash);
}

function keypress_fields(){
  $('#account_re, #account_price, #account_tcash').on('keyup keypress blur change', function(e) {
    compute();
  });

}