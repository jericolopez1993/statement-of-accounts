class CreateViralDatabase < ActiveRecord::Migration[5.2]
  def change
    create_table :viral_prices do |t|
      t.decimal :price, precision: 10, scale: 2
      t.integer :user_id
      t.timestamps null: false
    end
    create_table :viral_clients do |t|
      t.string :lname
      t.string :fname
      t.string :mi
      t.string :pnum
      t.string :tnum
      t.string :address
      t.integer :user_id
      t.timestamps null: false
    end
    create_table :viral_accounts do |t|
      t.integer :id_client
      t.integer :yr
      t.integer :mon
      t.integer :day
      t.integer :po
      t.integer :re
      t.integer :sold
      t.decimal :price, precision: 10, scale: 2
      t.decimal :tcash, precision: 10, scale: 2
      t.decimal :tamount, precision: 10, scale: 2
      t.decimal :bal, precision: 10, scale: 2
      t.string :date_paid
      t.binary :paid
      t.string :bank_name
      t.string :check_num
      t.date :date_issued
      t.string :rsnum
      t.string :prnum
      t.integer :addbal
      t.integer :user_id
      t.timestamps null: false
    end
    create_table :viral_archive_accounts do |t|
      t.integer :id_client
      t.integer :yr
      t.integer :mon
      t.integer :day
      t.integer :po
      t.integer :re
      t.integer :sold
      t.decimal :price, precision: 10, scale: 2
      t.decimal :tcash, precision: 10, scale: 2
      t.decimal :tamount, precision: 10, scale: 2
      t.decimal :bal, precision: 10, scale: 2
      t.string :date_paid
      t.binary :paid
      t.string :bank_name
      t.string :check_num
      t.date :date_issued
      t.string :rsnum
      t.string :prnum
      t.integer :addbal
      t.integer :user_id
      t.timestamps null: false
    end
    create_table :viral_archive_clients do |t|
      t.string :lname
      t.string :fname
      t.string :mi
      t.string :pnum
      t.string :tnum
      t.string :address
      t.integer :user_id
      t.timestamps null: false
    end
  end
end
