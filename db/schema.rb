# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2023_02_06_104250) do

  create_table "active_storage_attachments", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name", null: false
    t.string "record_type", null: false
    t.bigint "record_id", null: false
    t.bigint "blob_id", null: false
    t.datetime "created_at", null: false
    t.index ["blob_id"], name: "index_active_storage_attachments_on_blob_id"
    t.index ["record_type", "record_id", "name", "blob_id"], name: "index_active_storage_attachments_uniqueness", unique: true
  end

  create_table "active_storage_blobs", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "key", null: false
    t.string "filename", null: false
    t.string "content_type"
    t.text "metadata"
    t.bigint "byte_size", null: false
    t.string "checksum", null: false
    t.datetime "created_at", null: false
    t.index ["key"], name: "index_active_storage_blobs_on_key", unique: true
  end

  create_table "audits", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "auditable_id"
    t.string "auditable_type"
    t.integer "associated_id"
    t.string "associated_type"
    t.integer "user_id"
    t.string "user_type"
    t.string "username"
    t.string "action"
    t.text "audited_changes"
    t.integer "version", default: 0
    t.string "comment"
    t.string "remote_address"
    t.string "request_uuid"
    t.datetime "created_at"
    t.index ["associated_type", "associated_id"], name: "associated_index"
    t.index ["auditable_type", "auditable_id", "version"], name: "auditable_index"
    t.index ["created_at"], name: "index_audits_on_created_at"
    t.index ["request_uuid"], name: "index_audits_on_request_uuid"
    t.index ["user_id", "user_type"], name: "user_index"
  end

  create_table "kadyot_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.string "payment_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kadyot_archive_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kadyot_archive_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kadyot_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kadyot_prices", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "price", precision: 10, scale: 2
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kilatis_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kilatis_archive_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kilatis_archive_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kilatis_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "kilatis_prices", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "price", precision: 10, scale: 2
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "name"
    t.string "resource_type"
    t.bigint "resource_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["name", "resource_type", "resource_id"], name: "index_roles_on_name_and_resource_type_and_resource_id"
    t.index ["name"], name: "index_roles_on_name"
    t.index ["resource_type", "resource_id"], name: "index_roles_on_resource_type_and_resource_id"
  end

  create_table "sagad_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.string "payment_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sagad_archive_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sagad_archive_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sagad_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sagad_prices", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "price", precision: 10, scale: 2
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "first_name", default: "", null: false
    t.string "middle_name", default: "", null: false
    t.string "last_name", default: "", null: false
    t.string "gender", default: "", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string "current_sign_in_ip"
    t.string "last_sign_in_ip"
    t.string "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string "unconfirmed_email"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "authentication_token", limit: 30
    t.index ["authentication_token"], name: "index_users_on_authentication_token", unique: true
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "users_roles", id: false, options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "role_id"
    t.index ["role_id"], name: "index_users_roles_on_role_id"
    t.index ["user_id", "role_id"], name: "index_users_roles_on_user_id_and_role_id"
    t.index ["user_id"], name: "index_users_roles_on_user_id"
  end

  create_table "viral_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.string "payment_type"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "viral_archive_accounts", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.integer "id_client"
    t.integer "yr"
    t.integer "mon"
    t.integer "day"
    t.integer "po"
    t.integer "re"
    t.integer "sold"
    t.decimal "price", precision: 10, scale: 2
    t.decimal "tcash", precision: 10, scale: 2
    t.decimal "tamount", precision: 10, scale: 2
    t.decimal "bal", precision: 10, scale: 2
    t.string "date_paid"
    t.binary "paid"
    t.string "bank_name"
    t.string "check_num"
    t.date "date_issued"
    t.string "rsnum"
    t.string "prnum"
    t.integer "addbal"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "viral_archive_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "viral_clients", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.string "lname"
    t.string "fname"
    t.string "mi"
    t.string "pnum"
    t.string "tnum"
    t.string "address"
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "viral_prices", options: "ENGINE=InnoDB DEFAULT CHARSET=utf8", force: :cascade do |t|
    t.decimal "price", precision: 10, scale: 2
    t.integer "user_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_foreign_key "active_storage_attachments", "active_storage_blobs", column: "blob_id"
end
