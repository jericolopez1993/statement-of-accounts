# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)
User.create!([
                 {id: 1, email: "jerico.lopez1993@gmail.com", password: "rambo0702", password_confirmation: "rambo0702", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Jerico", last_name: "Lopez", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 2, email: "geraldine@jvgo.com", password: "geraldinepahoyo", password_confirmation: "geraldinepahoyo", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Geraldine", last_name: "Pahoyo", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 3, email: "helda@jvgo.com", password: "heldalopez", password_confirmation: "heldalopez", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Helda", last_name: "Lopez", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 4, email: "malyn@jvgo.com", password: "malyngo", password_confirmation: "malyngo", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Malyn", last_name: "Go", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil},
                 {id: 5, email: "rico@jvgo.com", password: "ricoraquel", password_confirmation: "ricoraquel", reset_password_token: nil, reset_password_sent_at: nil, remember_created_at: nil, created_at: "2019-09-11 06:09:58", updated_at: "2019-09-11 02:49:48", first_name: "Rico", last_name: "Raquel", confirmation_token: nil, confirmed_at: "2019-09-11 19:04:36", confirmation_sent_at: nil}
             ])
User.all.each do |user|
  if user.email == "jerico.lopez1993@gmail.com" || user.email == "malyn@jvgo.com"
      user.add_role :admin
  else
    user.add_role :user
  end
end


#ActiveRecord::Base.connection.tables.each do |table_name|
#  ActiveRecord::Base.connection.reset_sequence!(table_name)
#end