# Place in /config/puma/production.rb

rails_env = "production"
environment rails_env

app_dir = "/home/soa/apps/statement-of-accounts/shared" # Update me with your root rails app path
application_name = "statement-of-accounts"
released_path = "/home/soa/apps/statement-of-accounts/current"

bind  "unix://#{app_dir}/tmp/sockets/#{application_name}-puma.sock"
pidfile "#{app_dir}/tmp/pids/puma.pid"
state_path "#{app_dir}/tmp/pids/puma.state"
directory "#{app_dir}/"

stdout_redirect "#{app_dir}/log/puma.access.log", "#{app_dir}/log/puma.error.log", true

workers 2
threads 1,2

activate_control_app "unix://#{app_dir}/pumactl.sock"

prune_bundler