Rails.application.routes.draw do
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :kadyot do
    resources :clients
    resources :prices
    resources :accounts do
      collection do
        post  'search'
        post  'search_by_name'
        post  'search_by_month'
        get   'print'
        get   'generate_pdf'
        get   'print_sales_report'
        get   'latest_summary_report'
        get   'get_totals'
        get   'get_search_client'
      end
    end
  end

  namespace :sagad do
    resources :clients
    resources :prices
    resources :accounts do
      collection do
        post  'search'
        post  'search_by_name'
        post  'search_by_month'
        get   'print'
        get   'generate_pdf'
        get   'print_sales_report'
        get   'latest_summary_report'
        get   'get_totals'
        get   'get_search_client'
      end
    end
  end

  namespace :viral do
    resources :clients
    resources :prices
    resources :accounts do
      collection do
        post  'search'
        post  'search_by_name'
        post  'search_by_month'
        get   'print'
        get   'generate_pdf'
        get   'print_sales_report'
        get   'latest_summary_report'
        get   'get_totals'
        get   'get_search_client'
      end
    end
  end

  namespace :kilatis do
    resources :clients
    resources :prices
    resources :accounts do
      collection do
        post  'search'
        post  'search_by_name'
        post  'search_by_month'
        get   'print'
        get   'generate_pdf'
        get   'print_sales_report'
        get   'latest_summary_report'
        get   'get_totals'
        get   'get_search_client'
      end
    end
  end


  resources :reports do
    collection do
      post  'generate'
    end
  end

  devise_for :users
  devise_scope :user do
    get '/sign-in' => "devise/sessions#new", :as => :login
    authenticated :user do
      root 'sagad/clients#index', as: :authenticated_root
    end

    unauthenticated do
      root 'devise/sessions#new', as: :unauthenticated_root
    end
  end
  root 'sagad/clients#index'

  match '/users',   to: 'users#index',   via: 'get'
  match '/users/new',   to: 'users#new',   via: 'get'
  match '/users/:id/edit',   to: 'users#edit',   via: 'get',   as: 'edit_user'
  match '/users/create',   to: 'users#create',   via: 'post'
  match '/users/:id',   to: 'users#show',   via: 'get',   as: 'user'
  match '/users/:id',   to: 'users#update',   via: 'patch'
  match '/users/:id',   to: 'users#update',   via: 'put'
  match '/users/:id',   to: 'users#destroy',   via: 'delete'

end
