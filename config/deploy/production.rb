# Change these

server 'soa@localhost', password: fetch(:password), roles: [:web, :app, :db, :worker], primary: true

set :repo_url,        'https://jericolopez1993:p6uaWE8v6Qkt6F5vxKRf@bitbucket.org/jericolopez1993/statement-of-accounts.git'
set :application,     'statement-of-accounts'
set :user,            'soa'
set :password,        'passsword'
set :puma_threads,    [4, 16]
set :puma_workers,    0
set :branch, ENV.fetch('REVISION', 'master')
set :rvm_ruby_version, '2.6.0'
set :ssh_options, paranoid: false
set :sudo_password,    'password'
# set :ssh_options, { forward_agent: true, user: fetch(:user), keys: %w(~/.ssh/id_rsa.pub) }



# Don't change these unless you know what you're doing
set :pty,             true
set :use_sudo,        false
set :stage,           :production
set :deploy_via,      :remote_cache
set :deploy_to,       "/home/#{fetch(:user)}/apps/#{fetch(:application)}"
set :puma_bind,       "unix://#{shared_path}/tmp/sockets/#{fetch(:application)}-puma.sock"
set :puma_state,      "#{shared_path}/tmp/pids/puma.state"
set :puma_pid,        "#{shared_path}/tmp/pids/puma.pid"
set :puma_access_log, "#{release_path}/log/puma.error.log"
set :puma_error_log,  "#{release_path}/log/puma.access.log"
set :ssh_options,     { forward_agent: true, user: fetch(:user), password: fetch(:password), keepalive: true }
set :puma_preload_app, true
set :puma_worker_timeout, nil
set :puma_init_active_record, true  # Change to false when not using ActiveRecord
set :keep_releases, 1
set :linked_dirs, %w{public/system storage}
set :bundle_flags,      '--quiet' # this unsets --deployment, see details in config_bundler task details
set :bundle_path,       nil
set :bundle_without,    nil
